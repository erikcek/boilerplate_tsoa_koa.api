# Hacker New with custom collection maintenance


## Prerequisites
- Docker
- docker-compose

## Prerequisites for dev
- Docker
- docker-compose
- Node.js
- yarn

## Running:
 1. clone projects
 2. cd into project
 3. `docker-compose up -d` (if you are changing some code use `--build` flag)
 4. `docker-compose down` (for shutting down)

## Running for dev:
 1. clone projects
 2. cd into project
 3. `docker-compose -f docker-compose-dev.yml up -d`  (for database)
 4. create `.env` file based on `.env.sample` (default values shoud be sufficient)
 5. run `yarn`
 6. run `yarn dev`
 7. `rs` will restart nodemon manualy
 8. `ctrl + c` will kill nodemon deamon
 9. `docker-compose -f docker-compose-dev.yml down` (for shutting down)


## API documentation:
**swagger ui**: `http://localhost:9999/v1/docs` 

    - will list documentation with all available endpoints, (with interaction possibility)
    - make sure you authorize endpoints (`http://localhost:9999/v1/users/signUp` for creating new user -> `http://localhost:9999/v1/users/authorize` for returning jwt token)

**swagger.json**: `http://localhost:9999/v1/swagger` 

    - will return API json representation (might be usefull for frontent API SDK generation)


_(9999 is default port, make sure you replace port with your env settings)_

## Runnint tests:
1. run postgress instance (``docker-compose -f docker-compose-dev.yml up -d`` should be fine)
2. run `yarn` to install dependencies
3. run `yarn test` for running tests
4. `docker-compose -f docker-compose-dev.yml down` (for shutting down)



