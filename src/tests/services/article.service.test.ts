import { closeConnection, connect, getInstance } from "../../pg_sequelize";
import { UserInstance } from "../../pg_sequelize/User";
import * as articleService from "../../services/article.service";
import { DbInterface } from "../../typings/DbInterface";
import dotenv from "dotenv";
import { HNCollectionInstance } from "../../pg_sequelize/Collection";
import { HNArticle } from "../../typings/CustomTsoa";
import { HNArticleInstance } from "../../pg_sequelize/Article";
import { NotFoundError } from "../../helpers/error";

describe("Articl service tests", () => {
  const state: {
    user?: UserInstance;
    collection?: HNCollectionInstance;
    newArticle?: HNArticleInstance;
  } = {};
  let db: DbInterface | undefined = undefined;

  it("it should create new article", async (done) => {
    const articlesCount = await getInstance()?.Article.count();

    const newArticle: HNArticle = {
      hn_id: 314,
      title: "test_title",
      author: "test_author",
      url: "test_url",
      text: "test_text",
    } as HNArticle;

    const res = await articleService.createArticle(
      newArticle,
      state.collection?.id as number
    );

    const newArticleCount = await getInstance()?.Article.count();

    expect(res).toBeDefined();
    expect(res.id).toBeDefined();
    expect(res.title).toBe(newArticle.title);
    expect(res.author).toBe(newArticle.author);
    expect(res.url).toBe(newArticle.url);
    expect(res.text).toBe(newArticle.text);
    expect(newArticleCount).toBeGreaterThan(articlesCount as number);

    state.newArticle = res;

    done();
  });

  it("should recieve all user articles", async (done) => {
    const res = await articleService.getArticles(
      state.collection?.id as number
    );

    expect(res).toBeDefined();
    expect(res).toBeInstanceOf(Array);
    expect(res.length).toBeGreaterThanOrEqual(1);

    done();
  });

  it("should recieve one speccific article", async (done) => {
    const res = await articleService.getArticle(state.newArticle?.id as number);

    expect(res).toBeDefined();
    expect(res.id).toBe(state.newArticle?.id);
    expect(res.title).toBe(state.newArticle?.title);
    expect(res.author).toBe(state.newArticle?.author);
    expect(res.url).toBe(state.newArticle?.url);
    expect(res.text).toBe(state.newArticle?.text);
    done();
  });

  beforeAll(async () => {
    dotenv.config();
    const db = await connect({
      params: {
        host: "localhost",
        dialect: "postgres",
        operatorsAliases: false,
        logging: false,
      },
      database: process.env.PG_DATABASE,
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
    });

    const user = await db?.User.create({
      name: "testUser",
      password: "testPassword",
    });

    const collection = await db?.Collection.create({
      name: "test_collection",
    });
    state.user = user;
    state.collection = collection;
  });

  afterAll(async () => {
    await state.newArticle?.destroy();
    await state.user?.destroy();
    await state.collection?.destroy();
    await closeConnection();
  });
});
