import { closeConnection, connect, getInstance } from "../../pg_sequelize";
import { UserInstance } from "../../pg_sequelize/User";
import * as hackerNewsService from "../../services/hackerNews.service";
import { DbInterface } from "../../typings/DbInterface";
import dotenv from "dotenv";
import { HNCollectionInstance } from "../../pg_sequelize/Collection";
import { HNArticle } from "../../typings/CustomTsoa";
import { HNArticleInstance } from "../../pg_sequelize/Article";

describe("Hacker News service tests", () => {
  const state: {
    hnId: number;
    commentId: number;
  } = { hnId: 8863, commentId: 9224 };
  let db: DbInterface | undefined = undefined;

  it("should fetch one story", async (done) => {
    const story = await hackerNewsService.getStory(state.hnId);

    expect(story).toBeDefined();
    expect(story.hn_id).toBe(state.hnId);
    expect(story.url).toBeDefined();
    expect(story.text).toBeUndefined();

    done();
  });

  it("should fetch multile sotires story", async (done) => {
    const stories = await hackerNewsService.getMultipleStories([state.hnId]);

    expect(stories).toBeDefined();
    expect(stories).toBeInstanceOf(Array);
    expect(stories.length).toBe(1);

    done();
  });

  it("should fetch one comment", async (done) => {
    const comment = await hackerNewsService.getComment(state.commentId);

    expect(comment).toBeDefined();
    expect(comment.hn_id).toBe(state.commentId);
    expect(comment.parent_id).toBe(state.hnId);

    done();
  });

  beforeAll(async () => {
    dotenv.config();
  });

  afterAll(async () => {});
});
