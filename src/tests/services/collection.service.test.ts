import { closeConnection, connect } from "../../pg_sequelize";
import { UserInstance } from "../../pg_sequelize/User";
import * as collectionService from "../../services/collection.service";
import { DbInterface } from "../../typings/DbInterface";
import dotenv from "dotenv";
import { HNCollectionInstance } from "../../pg_sequelize/Collection";
import { HttpError } from "../../helpers/error";

describe("Collection service tests", () => {
  const state: {
    user?: UserInstance;
    newCollection?: HNCollectionInstance;
  } = {};
  let db: DbInterface | undefined = undefined;

  it("it should create new collection", async (done) => {
    const newCollection = {
      name: "test_collection",
    };
    const res = await collectionService.createCollection(
      newCollection,
      state.user?.id as number
    );

    // const resJson = await res.toJSON();

    expect(res).toHaveProperty("name", newCollection.name);
    expect(res.id).toBeDefined();
    expect(res.createdAt).toBeDefined();
    expect(res.updatedAt).toBeDefined();

    state.newCollection = res;

    done();
  });

  it("should recieve all collections", async (done) => {
    const res = await collectionService.getCollections(
      state.user?.id as number
    );

    expect(res).toBeDefined();
    expect(res).toBeInstanceOf(Array);
    expect(res.length).toBeGreaterThanOrEqual(1);
    done();
  });

  it("should recieve one speccific collection", async (done) => {
    const res = await collectionService.getCollection(
      state.newCollection?.id as number,
      state.user?.id as number
    );

    expect(res).toBeDefined();
    expect(res.id).toBe(state.newCollection?.id);
    expect(res.name).toBe(state.newCollection?.name);
    done();
  });

  it("should update collection", async (done) => {
    const newCollection = {
      name: "new_test_collection_name",
    };
    const res = await collectionService.updateCollection(
      state.newCollection?.id as number,
      newCollection,
      state.user?.id as number
    );

    expect(res).toBeDefined();
    expect(res.id).toBe(state.newCollection?.id);
    expect(res.name).toBe(newCollection.name);
    done();
  });

  it("should delete one collection", async (done) => {
    const allCollectionsBefore = await collectionService.getCollections(
      state.user?.id as number
    );

    const idsBefore = allCollectionsBefore.map((i) => i.id);

    expect(idsBefore).toContain(state.newCollection?.id);

    const res = await collectionService.removeCollection(
      state.newCollection?.id as number,
      state.user?.id as number
    );

    expect(res).toBeUndefined();

    const allCollectionsAfter = await collectionService.getCollections(
      state.user?.id as number
    );

    const idsAfter = allCollectionsAfter.find(
      (i) => i.id === state.newCollection
    );
    expect(idsAfter).toBeUndefined();
    done();
  });

  it("should not found collection asn throw NotFoundError", async (done) => {
    await expect(
      collectionService.getCollection(
        state.newCollection?.id as number,
        state.user?.id as number
      )
    ).rejects.toThrow(HttpError);

    done();
  });

  beforeAll(async () => {
    dotenv.config();
    const db = await connect({
      params: {
        host: "localhost",
        dialect: "postgres",
        operatorsAliases: false,
        logging: false,
      },
      database: process.env.PG_DATABASE,
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
    });

    const user = await db?.User.create({
      name: "testUser",
      password: "testPassword",
    });
    state.user = user;
  });

  afterAll(async () => {
    await state.user?.destroy();
    await closeConnection();
  });
});
