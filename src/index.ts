import dotenv from "dotenv";
import * as winstonLogger from "./helpers/logger";
import { createServer } from "./server";

dotenv.config();

const preconfigure = async () => {
  // creating basic winston logger

  winstonLogger.createWinstonLogger();
  const winstonInstance = winstonLogger.getInstance();

  // connecting to postgress
  try {
    // add connectin do DB
  } catch (error) {
    process.exit(1);
  }

};

preconfigure().then((res) => {
  console.log(`Starting server...`);
  createServer();
  console.log(`Server is listening on port ${process.env.PORT}`);
});
