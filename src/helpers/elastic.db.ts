import { Client } from "@elastic/elasticsearch";

export interface ElasticSearchState {
  state?: Client;
}

export let elasticSearchState: ElasticSearchState = {
  state: undefined,
};

export const connect = (host: string, port: number) => {
  if (elasticSearchState.state === undefined) {
    const client = new Client({ node: `${host}:${port}` });
    elasticSearchState.state = client;
  }
  return elasticSearchState.state;
};

export const getInstance = () => {
  return elasticSearchState.state;
};
