// Custom error classes, that does not depend on framework
// therefore servicess can use throw instead of ctx.throw (for koa)
// and custom middleware handles respons acordigly
// Services are therefore platform independand
// TODO: check performance

export class HttpError extends Error {
  statusCode: number;
  statusMessage: string;
  description?: string;

  constructor(statusCode: number, statusMessage: string, description?: string) {
    super(statusMessage);
    this.statusCode = statusCode;
    this.statusMessage = statusMessage;
    this.description = description;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, HttpError.prototype);
  }
}

export class NotFoundError extends HttpError {
  static displayName = "Not Found";
  constructor(statusMessage?: string, description?: string) {
    super(404, statusMessage || "Not Found", description);
  }
}

export class UnathorizedError extends HttpError {
  static displayName = "Unathorized";
  constructor(statusMessage?: string, description?: string) {
    super(401, statusMessage || "Unathorized", description);
  }
}

export class InteralServerError extends HttpError {
  static displayName = "Internal Server Error";
  constructor(statusMessage?: string, description?: string) {
    super(500, statusMessage || "Internal Server Error", description);
  }
}

export class ForbiddenError extends HttpError {
  static displayName = "Forbidden";
  constructor(statusMessage?: string, description?: string) {
    super(403, statusMessage || "Forbidden", description);
  }
}
