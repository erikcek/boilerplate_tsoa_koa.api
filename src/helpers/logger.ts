import winston, { createLogger } from "winston";

const state: { logger?: winston.Logger } = {
  logger: undefined,
};

export const createWinstonLogger = (): void => {
  if (state.logger) {
    return;
  }
  const customLogger = createLogger({
    level: "info",
    format: winston.format.simple(),
    transports: [
      new winston.transports.File({
        filename: "logs/error.log",
        level: "error",
      }),
      new winston.transports.File({ filename: "logs/combined.log" }),
    ],
  });
  state.logger = customLogger;
};

export const getInstance = () => {
  return state.logger;
};
